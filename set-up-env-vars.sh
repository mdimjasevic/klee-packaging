#!/bin/bash

# KLEE packaging values
package=klee
org=$package
upstream_version=1.1.0
version="${upstream_version}"
package_version=${package}-${version}
# package revision
package_revision="1"
package_dir=${package_version}
package_dir_absolute=~/${package_version}
archive_ext=tar.gz
archive_name="${package_version}.${archive_ext}"

# Upstream values
upstream_name=klee
upstream_archive=${upstream_name}-${upstream_version}.tar.gz
upstream_dir=${upstream_name}-v${upstream_version}
