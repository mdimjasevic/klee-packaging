#!/bin/bash

set -e

tools_path=/vagrant

# Load various packaging values
source ${tools_path}/set-up-env-vars.sh

# Install and configure various Debian build tools
source ${tools_path}/set-up-basics.sh

# Install KLEE dependencies
source ${tools_path}/install-tool-deps.sh

# Get, build, and install STP without shared libraries as KLEE authors
# ask for that
# source ${tools_path}/build-stp.sh

# If it is to be judged based on test cases in KLEE, an STP version
# with shared libraries works just fine
stp_package=${tools_path}/stp_2.1.1+dfsg-1_amd64.deb
sudo dpkg -i ${stp_package}

# The maximum stack size in this bash shell
ulimit -s unlimited

# Put KLEE-uClibc into the VM. KLEE-uClibc is needed to analyze real
# programs (unlike simplistic closed programs).
klee_uclibc_archive="${package}_${version}.orig-klee-uclibc.tar.gz"
klee_uclibc_archive_dir=`dirname ${package_dir_absolute}`
cp /vagrant/${klee_uclibc_archive} ${klee_uclibc_archive_dir}

# This is now handled through debian/rules
# source ${tools_path}/build-klee-uclibc.sh

# Build libgtest
# source ${tools_path}/build-libgtest.sh


# The meat part with getting and building KLEE

# Get KLEE and unpack it
cd
wget https://github.com/${org}/${package}/archive/v${upstream_version}.tar.gz -O ${upstream_archive}

tar xf ${upstream_archive}

# Start packaging KLEE
export DEBEMAIL="marko@dimjasevic.net"
export DEBFULLNAME="Marko Dimjašević"
extra_exclude="--exclude=.gitignore"
# Important: don't tar by providing an absolute path to the target!
tar czf ${archive_name} --exclude-vcs ${extra_exclude} ${package_dir}

cd ${package_dir_absolute}
dh_make --single --yes --file ../${archive_name}

# Update debian/control
cp /vagrant/debian/control ${package_dir_absolute}/debian/

# Update debian/copyright
# cp /vagrant/debian/copyright ${package_dir_absolute}/debian/

# Update debian/rules
cp /vagrant/debian/rules ${package_dir_absolute}/debian/

# Apply a patch that fixes the build system
patch_p1=fix-makefile-llvm-3.4.patch
quilt import -P ${patch_p1} /vagrant/debian/patches/${patch_p1}
quilt push -a

exit 0

klee_upstream_name=v${upstream_version}.tar.gz
wget https://github.com/klee/klee/archive/${klee_upstream_name}
tar xf ${klee_upstream_name}
cd klee-${upstream_version}
# Apply a patch that fixes Makefile.common for LLVM 3.4
./configure -prefix=/usr --with-llvmsrc=/usr/lib/llvm-3.4/build --with-llvmobj=/usr/lib/llvm-3.4/build --with-llvmcc=/usr/bin/clang-3.4 --with-llvmcxx=/usr/bin/clang++-3.4 --with-uclibc=/home/vagrant/klee-uclibc-klee_uclibc_v${upstream_version}/ --enable-posix-runtime --with-stp=/usr

# Add this to Makefile.common
CFLAGS := $(filter-out -fstack-protector-strong,$(CFLAGS))
CXXFLAGS := $(filter-out -fstack-protector-strong,$(CXXFLAGS))
