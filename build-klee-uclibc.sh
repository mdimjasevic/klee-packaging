#!/bin/bash

echo "In build-klee-uclibc.sh"

# Get KLEE-uClibc and build it
cd
klee_uclibc_archive="${package}_${version}.orig-klee-uclibc.tar.gz"
klee_uclibc_archive_dir=$(dirname ${package_dir_absolute})
echo "klee_uclibc_archive: ${klee_uclibc_archive}"
echo "package_dir_absolute: ${package_dir_absolute}"
echo "klee_uclibc_archive_dir: ${klee_uclibc_archive_dir}"
echo "tools_path: ${tools_path}"

cp ${tools_path}/${klee_uclibc_archive} ${klee_uclibc_archive_dir}

# klee_uclibc_upstream_name=klee_uclibc_v1.0.0.tar.gz
# wget https://github.com/klee/klee-uclibc/archive/${klee_uclibc_upstream_name}
tar xf ${klee_uclibc_archive}
cd klee-uclibc-klee_uclibc_v1.0.0
./configure --with-llvm-config=/usr/bin/llvm-config-3.4 --make-llvm-lib
# From https://klee.github.io/build-llvm34/: "NOTE: If you are on a
# different target (i.e., not i386 or x64), you will need to run make
# config and select the correct target. The defaults for the other
# uClibc configuration variables should be fine."
make -j8 KLEE_CFLAGS="-DKLEE_SYM_PRINTF"
