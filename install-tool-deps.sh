#!/bin/bash

# Installs dependencies for experimental KLEE based on LLVM 3.4
dependencies="clang-3.4 llvm-3.4 llvm-3.4-dev llvm-3.4-tools libncurses5-dev curl git bison flex bc libcap-dev cmake libboost-program-options1.55.0 python-minimal unzip minisat libgtest-dev autotools-dev python-tabulate python-setuptools"
sudo apt-get install --yes $dependencies
