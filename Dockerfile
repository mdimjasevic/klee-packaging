FROM jessie:amd64
MAINTAINER Marko Dimjašević <marko@dimjasevic.net>

ENV DEBIAN_FRONTEND noninteractive

ENV home_dir /home/docker

# KLEE packaging values
ENV package klee
ENV org $package
ENV upstream_version 1.2.0
ENV version $upstream_version
ENV package_version $package-$version
# package revision
ENV package_revision 1
ENV package_dir $package_version
ENV package_dir_absolute "${home_dir}/${package_version}"
ENV archive_ext "tar.gz"
ENV archive_name "${package_version}.${archive_ext}"
# Upstream values
ENV upstream_name klee
ENV upstream_archive ${upstream_name}-${upstream_version}.${archive_ext}
ENV upstream_dir ${upstream_name}-v${upstream_version}

# Configure apt-get caching
# RUN /sbin/ip route | awk '/default/ { print "Acquire::http::Proxy \"http://"$3":8000\";" }' > /etc/apt/apt.conf.d/30proxy

# Add Jessie Backports
RUN echo "deb http://mirrors.kernel.org/debian jessie-backports main" >> /etc/apt/sources.list

# Install needed basics not present in the Docker image
RUN apt-get update
RUN apt-get install --yes apt-utils
RUN apt-get install --yes bash wget sudo locales
# Create OpenSSH privilege separation directory
RUN mkdir -p /var/run/sshd

# Create a sudo user "docker"
RUN adduser --disabled-password docker
RUN adduser docker sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
# RUN su --preserve-environment --command /tmp/set-up-basics.sh docker

# Set up basics for packaging
ADD set-up-basics.sh /tmp/
RUN su --command /tmp/set-up-basics.sh docker

# Install KLEE dependencies
ADD install-tool-deps.sh /tmp/
RUN su --command /tmp/install-tool-deps.sh docker
# Install STP as a dependency
ENV stp_package stp_2.1.1+dfsg-1_amd64.deb
ADD ${stp_package} /tmp/
RUN dpkg -i /tmp/${stp_package}

# Locale stuff
RUN dpkg-reconfigure locales && \
    locale-gen C.UTF-8 && \
    /usr/sbin/update-locale LANG=C.UTF-8
RUN echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen && \
    locale-gen
ENV LC_ALL C.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

# Current dir and user
WORKDIR ${home_dir}
USER docker

# Put KLEE-uClibc into the image. KLEE-uClibc is needed to analyze
# real programs (unlike simplistic closed programs).
ENV klee_uclibc_archive "${package}_${version}.orig-klee-uclibc.tar.gz"
COPY ${klee_uclibc_archive} ${home_dir}/
ENV klee_uclibc_conf_archive "${package}_${version}.orig-klee-uclibc-conf.tar.gz"
COPY ${klee_uclibc_conf_archive} ${home_dir}/

# Get KLEE and unpack it
RUN wget https://github.com/${org}/${package}/archive/v${upstream_version}.tar.gz -O ${upstream_archive}
RUN tar xf ${home_dir}/${upstream_archive}

# Start packaging KLEE
ENV DEBEMAIL "marko@dimjasevic.net"
ENV DEBFULLNAME "Marko Dimjašević"
ENV extra_exclude "--exclude=.gitignore"
# Important: don't tar by providing an absolute path to the target!
RUN tar czf ${archive_name} --exclude-vcs ${extra_exclude} ${package_dir}

WORKDIR ${package_dir_absolute}
RUN dh_make --single --yes --file ../${archive_name}

# Update debian/control
COPY debian/control ${package_dir_absolute}/debian/

# Update debian/copyright
COPY debian/copyright ${package_dir_absolute}/debian/

# Update debian/rules
COPY debian/rules ${package_dir_absolute}/debian/

# Update debian/changelog
COPY debian/changelog ${package_dir_absolute}/debian/

# Remove the generated debian/README.Debian file as it is not needed
RUN rm ${package_dir_absolute}/debian/README.Debian

# Remove README.source as we don't need it
RUN rm ${package_dir_absolute}/debian/README.source

# Copy a file documenting documentation files
COPY debian/docs ${package_dir_absolute}/debian/

# Copy man pages
COPY debian/*.1 ${package_dir_absolute}/debian/
COPY debian/klee.manpages ${package_dir_absolute}/debian/

# Copy menu files
COPY debian/*.menu ${package_dir_absolute}/debian/

# Copy a file telling where examples are
COPY debian/klee.examples ${package_dir_absolute}/debian/

# Copy watch (used by uscan(1))
COPY debian/watch ${package_dir_absolute}/debian/

# Remove all example files from debian/
RUN rm --force ${package_dir_absolute}/debian/*.ex
RUN rm --force ${package_dir_absolute}/debian/*.EX

# Apply a patch that fixes the build system
ENV patch_p2 "remove-failing-tests.patch"
ENV patch_p3 "remove-rpath.patch"
COPY debian/patches/${patch_p2} /tmp/
COPY debian/patches/${patch_p3} /tmp/
RUN quilt import -P ${patch_p2} /tmp/${patch_p2}
RUN quilt import -P ${patch_p3} /tmp/${patch_p3}
RUN quilt push -a

# Change file permissions of copied files
USER root
RUN chown docker:docker ${home_dir}/${klee_uclibc_archive}
RUN chown docker:docker ${home_dir}/${klee_uclibc_conf_archive}
RUN chown --recursive docker:docker ${package_dir_absolute}
USER docker
