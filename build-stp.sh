#!/bin/bash

# STP
cd
git clone https://github.com/stp/stp.git
mkdir stp/build
cd stp/build
cmake -DENABLE_TESTING=OFF -DTEST_APIS=OFF -DTEST_C_API=OFF -DTEST_QUERY_FILES=OFF -DLIT_TOOL=/usr/lib/llvm-3.4/build/utils/lit/lit.py -DENABLE_PYTHON_INTERFACE=OFF -DBUILD_SHARED_LIBS=OFF -DALSO_BUILD_STATIC_LIB=ON -DCMAKE_BUILD_TYPE=RelWithDebInfo -DENABLE_ASSERTIONS=ON -DCMAKE_INSTALL_PREFIX=/usr ..
make
sudo make install
